<?php

namespace GCSEPod\Apps;

require('vendor/autoload.php');

use Ramsey\Uuid\Uuid;
use SQLite3;

class Calc
{
    const SQLITE_DB_FILENAME = "test_db";

    static function input($input)
    {
        // No input - means we must be showing our summary
        if (empty($input)) {

            // Render an HTML table of all events logged

            $db = new SQLite3(self::SQLITE_DB_FILENAME );
            $sql = "SELECT * FROM EVENTS";
            $result = $db->query($sql);
            echo '<table cellspacing="5" border="1">';
            echo '<tr><td>ID</td><td>Session</td><td>Event</td><td>Created At</td></tr>';
            while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
                echo '<tr>';
                echo '<td>';
                echo implode('</td><td>', $row);
                echo '</td>';
                echo '</tr>';
            }
            echo '</table>';
        } else {
            if ( array_key_exists( 'newSession', $input ) ) {
                echo (string) Uuid::uuid4();
            } else {

                // Log event to file-based DB

                $db = new SQLite3( self::SQLITE_DB_FILENAME );
                $sql = "CREATE TABLE IF NOT EXISTS EVENTS
                    (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    `session` VARCHAR(100) NOT NULL,
                    `event` TEXT NOT NULL,
                    created_at DATETIME NOT NULL);";
                $db->exec($sql);

                $smt = $db->prepare( "INSERT INTO EVENTS (`session`,`event`,`created_at`) VALUES (?,?, ". time() .")" );
                $smt->bindValue( 1, $input['session'] );
                $smt->bindValue( 2, $input['input'] );
                $smt->execute();
            }
        }
    }
}

Calc::input($_POST);
